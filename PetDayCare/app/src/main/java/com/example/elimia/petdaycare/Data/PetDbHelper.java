package com.example.elimia.petdaycare.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
/* importar la clase pet entri
 */
import com.example.elimia.petdaycare.Data.PetContract.PetEntry;
import com.example.elimia.petdaycare.Pet;

import java.util.ArrayList;


public class PetDbHelper extends SQLiteOpenHelper {
    /*
    nombre de la base de datos
     */
    private static final String DATABASE_NAME = "pet.db";
    /*
    VERSION ACTUAL DE LA BASE DE DATOS SI CAMBIAMOS EL ESQUEMA TENDREMOS QUE ACTUALIZAR ESTE NUMERO
     */
    private static final int DATABASE_VERSION = 1;

    public PetDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION); }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_PETS_TABLE = "CREATE TABLE " + PetEntry.TABLE_NAME +
                "("
                + PetEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PetEntry.COLUMN_PET_NAME + " TEXT NOT NULL, "
                + PetEntry.COLUMN_PET_BREED + " TEXT NOT NULL, "
                + PetEntry.COLUMN_PET_WEIGHT + " INTEGER NOT NULL DEFAULT 0, "
                + PetEntry.COLUMN_PET_GENDER + " TEXT NOT NULL);";
        sqLiteDatabase.execSQL(SQL_CREATE_PETS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public long insertPet(Pet mascota){
        SQLiteDatabase bd = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PetEntry.COLUMN_PET_NAME, mascota.getNombre());
        contentValues.put(PetEntry.COLUMN_PET_BREED, mascota.getRaza());
        contentValues.put(PetEntry.COLUMN_PET_GENDER, mascota.getGenero());
        contentValues.put(PetEntry.COLUMN_PET_WEIGHT, mascota.getPeso());
        long newRowId = bd.insert(PetEntry.TABLE_NAME,null,contentValues);
        return newRowId;
    }
    /*
    la proyeccion que nos va a mostrar todos los datos
     */
    public ArrayList <Pet> displayDatabaseInfo(){
        ArrayList <Pet> myPet = new ArrayList <Pet>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String [] projection = {
                PetEntry._ID,
                PetEntry.COLUMN_PET_NAME,
                PetEntry.COLUMN_PET_BREED,
                PetEntry.COLUMN_PET_GENDER,
                PetEntry.COLUMN_PET_WEIGHT
        };
        Cursor cursor = bd.query(
                PetEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        //obtenemos los indices de nuestras columnas
        int nameColum = cursor.getColumnIndex(PetEntry.COLUMN_PET_NAME);
        int razaColum = cursor.getColumnIndex(PetEntry.COLUMN_PET_BREED);
        int pesoColum = cursor.getColumnIndex(PetEntry.COLUMN_PET_WEIGHT);
        int generoColum = cursor.getColumnIndex(PetEntry.COLUMN_PET_GENDER);

        //con cada uno de los indices ya podemos recorrer las filas

        while (cursor.moveToNext()){
            String currentName = cursor.getString(nameColum);
            String currentRaza = cursor.getString(razaColum);
            int currentpeso = cursor.getInt(pesoColum);
            String currentgenero = cursor.getString(generoColum);
            if (currentName.isEmpty() || currentRaza.isEmpty() || Integer.toString(currentpeso).isEmpty()){
                myPet.add(null);
            }else{
                Pet currentPet = new Pet(currentName,currentRaza,Integer.toString(currentpeso) ,currentgenero);
                myPet.add(currentPet);
            }
        }
        return myPet;
    }

    public  void editarPet(String nombre,String raza,String genero ,String peso ,int position){
        SQLiteDatabase bd =getWritableDatabase();
        if (bd!=null){
            bd.execSQL("UPDATE "+PetEntry.TABLE_NAME+ "SET"+PetEntry.COLUMN_PET_NAME+" = '"+nombre+"', "+PetEntry.COLUMN_PET_BREED+" = '"+raza+"', "+PetEntry.COLUMN_PET_WEIGHT+" = '"+peso+"' ,"+PetEntry.COLUMN_PET_GENDER+" = '"+genero+"' WHERE "+PetEntry._ID+" = "+position);
            bd.close();
        }
    }

    public void borrarPet(int position){
        SQLiteDatabase bd =getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM "+PetEntry.TABLE_NAME+" WHERE "+PetEntry._ID+" = "+position);
            bd.close();
        }
    }

}
