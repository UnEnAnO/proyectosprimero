package com.example.elimia.petdaycare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.elimia.petdaycare.Data.PetContract;
import com.example.elimia.petdaycare.Data.PetDbHelper;

import java.util.ArrayList;

public class createPet extends AppCompatActivity {
    private Spinner spinnerceratePet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pet);
        final EditText et_namePet = findViewById(R.id.nombreFichaMascotaCrear);
        final EditText et_razaPet = findViewById(R.id.razaFichaMascotaCrear);
        final EditText et_pesoPet = findViewById(R.id.pesoFichaMascotaCrear);
        Button btn_crear = findViewById(R.id.btn_crear);
        spinnerceratePet = (Spinner) findViewById(R.id.spinnerCreatePet);
        ArrayList<String> generos2 = new ArrayList<>();

        generos2.add("Masculino");
        generos2.add("Femenino");
        ArrayAdapter adp = new ArrayAdapter(createPet.this, android.R.layout.simple_spinner_dropdown_item, generos2);

        spinnerceratePet.setAdapter(adp);
        spinnerceratePet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerceratePet.setOnItemSelectedListener(this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        btn_crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emty_errors = "";
                if (et_namePet.getText() == null || et_namePet.getText().toString().equalsIgnoreCase("")) {
                    emty_errors += "-El nombre no puede estar vacio\n";
                }
                if (et_razaPet.getText() == null || et_razaPet.getText().toString().equalsIgnoreCase("")) {
                    emty_errors += "-La raza no puede estar vacio\n";
                }
                if (et_pesoPet.getText() == null || et_pesoPet.getText().toString().equalsIgnoreCase("")) {
                    emty_errors += "-la edad no puede estar vacia\n";
                }
                if (emty_errors.equals("")) {
                    callInsertNewPet(et_namePet.getText().toString(),et_razaPet.getText().toString(),et_pesoPet.getText().toString());
                    Intent i =new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), emty_errors,
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    /*
        este metodo sera llamado por el boton de crear
         */
    public void callInsertNewPet(String namePET, String razaPET,  String pesoPET){
        /*
        coger los campos del formulario y verificar que no esten vacios y se los pasamos por parametro min 14 del p32
         */
        String spiner = spinnerceratePet.getSelectedItem().toString();
        Pet pet = new Pet (namePET,razaPET,pesoPET,spiner);
        PetDbHelper petDbHelper = new PetDbHelper(this);
        double test = petDbHelper.insertPet(pet);
        Log.i("este es el id de la nueva columna",""+test);
    }
}
