package com.example.elimia.petdaycare;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PetAdapter extends ArrayAdapter<Pet> {
    public PetAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Pet> mascotas) {
        super(context, resource, mascotas);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View list_item = convertView;
        //verificamos si la vista esta vacia para en caso afirmativo inflarla
        if(list_item == null){
            list_item = LayoutInflater.from(getContext()).inflate(R.layout.item_list, parent, false);
        }

        //nos traemos cada una de las mascotas de nuestro Array
        Pet currentPet = getItem(position);
        TextView nombreMascota = (TextView) list_item.findViewById(R.id.nombreMascota);
        nombreMascota.setText(currentPet.getNombre());
        TextView razaMascota = (TextView) list_item.findViewById(R.id.razaMascota);
        razaMascota.setText(currentPet.getRaza());

        return list_item;
    }
}
