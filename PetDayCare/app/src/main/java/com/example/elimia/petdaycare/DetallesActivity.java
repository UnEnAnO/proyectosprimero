package com.example.elimia.petdaycare;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.example.elimia.petdaycare.Data.PetContract;
import com.example.elimia.petdaycare.Data.PetDbHelper;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class DetallesActivity extends AppCompatActivity {
    private Spinner spinnerDetalles;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        spinnerDetalles = (Spinner) findViewById(R.id.spinnerDetalles);
        Button btn_guardar = findViewById(R.id.guardarPet);
        Button btn_borrar = findViewById(R.id.borrarPet);
        ArrayList<String> generos =new ArrayList<>();

        generos.add("Masculino");
        generos.add("Femenino");
        ArrayAdapter adp = new ArrayAdapter(DetallesActivity.this, android.R.layout.simple_spinner_dropdown_item,generos);

        spinnerDetalles.setAdapter(adp);
        spinnerDetalles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        String hintNombre = getIntent().getStringExtra("nombrePET");
        final EditText PetName = (EditText) findViewById(R.id.nombreFichaMascota);
        PetName.setHint(hintNombre);
        String hintRaza = getIntent().getStringExtra("razaPET");
        final EditText PetRaza = (EditText) findViewById(R.id.razaFichaMascota);
        PetRaza.setHint(hintRaza);
        String spinnergenero = getIntent().getStringExtra("generoPET");
        if (spinnergenero.equalsIgnoreCase("Masculino")){
            spinnerDetalles.setSelection(0);
        }
        if (spinnergenero.equalsIgnoreCase("Femenino")){
            spinnerDetalles.setSelection(1);
        }

        String hintPeso = getIntent().getStringExtra("pesoPET");
        final EditText PetPeso = (EditText) findViewById(R.id.pesoFichaMascota);
        PetPeso.setHint(hintPeso);
        String posicion = getIntent().getStringExtra("posicionPET");
        final int posicion2 = Integer.valueOf(posicion);

/*
        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              callEditPet(PetName.getText().toString(), PetRaza.getText().toString(), spinnerDetalles.getSelectedItem().toString(),PetPeso.toString(),posicion2);
            }
        });
        */


        btn_borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeletePet(posicion2);
            }
        });
    }



    public void callEditPet(String namePET, String razaPET, String genero, String pesoPET, int posicion){
        /*
        coger los campos del formulario y verificar que no esten vacios y se los pasamos por parametro min 14 del p32
         */
        PetDbHelper petDbHelper = new PetDbHelper(this);
        petDbHelper.editarPet(namePET,razaPET,genero,pesoPET,posicion);
        Intent i =new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
    }
    public void callDeletePet(int posicion){
        /*
        coger los campos del formulario y verificar que no esten vacios y se los pasamos por parametro min 14 del p32
         */
        PetDbHelper petDbHelper = new PetDbHelper(this);
        petDbHelper.borrarPet(posicion);
        Intent i =new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
    }

}
