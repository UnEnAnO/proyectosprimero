package com.example.elimia.petdaycare;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
/* importar la clase pet entri
 */
import com.example.elimia.petdaycare.Data.PetContract;
import com.example.elimia.petdaycare.Data.PetContract.PetEntry;
import com.example.elimia.petdaycare.Data.PetDbHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Pet> myPetList = new ArrayList<Pet>();

    ImageView createPet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PetDbHelper mDbHelper = new PetDbHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        createPet = (ImageView) findViewById(R.id.createPet);
        ListView list = (ListView) findViewById(R.id.listMascotas);
        myPetList  = mDbHelper.displayDatabaseInfo();

        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getApplicationContext(),DetallesActivity.class);
                        i.putExtra("nombrePET", myPetList.get(position).getNombre());
                        i.putExtra("razaPET", myPetList.get(position).getRaza());
                        i.putExtra("generoPET", myPetList.get(position).getGenero());
                        i.putExtra("pesoPET", myPetList.get(position).getPeso());
                        startActivity(i);
            }
        };

        createPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(),createPet.class);
                startActivity(i);
            }
        });

        PetAdapter PetAdapter = new PetAdapter(this, 0, myPetList);
        list.setAdapter(PetAdapter);
        list.setOnItemClickListener(onclick);
    }


}
