package com.example.elimia.petdaycare;

public class Pet {
    String nombre;
    String raza;
    String peso;
    String genero;

    public Pet(String nombre, String raza, String peso, String genero) {
        this.nombre = nombre;
        this.raza = raza;
        this.peso = peso;
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
