package com.example.fantasticanimals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fantasticanimals.Data.CriatureContract;
import com.example.fantasticanimals.Data.CriatureDBHeLPER;

import java.util.ArrayList;

public class createAnimal extends AppCompatActivity {
    private Spinner spinnerCreateCriature;
    int counter =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_animal);
        final EditText et_nameCreateCriature =findViewById(R.id.NombreFichaCriaturaCrear);
        final EditText et_descriptionCreateCriature =findViewById(R.id.descripcionEditCrear);
        Button btn_plus = findViewById(R.id.btn_MasCrear);
        Button btn_minus = findViewById(R.id.btn_MenosCrear);
        Button btn_createCriature = findViewById(R.id.btn_crear);
        Button btn_pintar = findViewById(R.id.btn_pintar);
       final TextView numeroText = findViewById(R.id.nivelCrear);
        spinnerCreateCriature = (Spinner) findViewById(R.id.spinnerTipoCrear);
        ArrayList<String> tipos = new ArrayList<>();

        tipos.add("Amigable");
        tipos.add("Neutral");
        tipos.add("Peligrosa");
        tipos.add("Potencialmente Peligrosa");
        ArrayAdapter adp = new ArrayAdapter(createAnimal.this, android.R.layout.simple_spinner_dropdown_item, tipos);

        spinnerCreateCriature.setAdapter(adp);
        spinnerCreateCriature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerCreateCriature.setOnItemSelectedListener(this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (counter>1){
                    counter=counter-1;
                    numeroText.setText(counter+"");
                }else{
                    Toast.makeText(getApplicationContext(), "El valor ha de estar entre 1 y 5.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (counter<5){
                    counter=counter+1;
                    numeroText.setText(counter+"");
                }else{
                    Toast.makeText(getApplicationContext(), "El valor ha de estar entre 1 y 5.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });



        btn_createCriature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emty_errors = "";
                if (et_nameCreateCriature.getText() == null || et_nameCreateCriature.getText().toString().equalsIgnoreCase("")) {
                    emty_errors += "-El nombre de la criatura no puede estar vacio\n";
                }
                if (et_descriptionCreateCriature.getText() == null || et_descriptionCreateCriature.getText().toString().equalsIgnoreCase("")) {
                    emty_errors += "-El nombre de la criatura no puede estar vacio\n";
                }
                if (emty_errors.equals("")) {
                    callInsertNewCriature(et_nameCreateCriature.getText().toString(),et_descriptionCreateCriature.getText().toString(),numeroText.getText().toString());
                    Intent i =new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), emty_errors,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_pintar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(),paint.class);
                startActivity(i);
            }
        });


    }
    public void callInsertNewCriature(String et_nameCreateCriature, String et_descriptionCreateCriature ,String numeroText) {

        String spinner = spinnerCreateCriature.getSelectedItem().toString();
        Criature criature = new Criature(et_nameCreateCriature,Integer.parseInt(numeroText),spinner,et_descriptionCreateCriature);
        CriatureDBHeLPER criatureDBHeLPER = new CriatureDBHeLPER(this);
        double test = criatureDBHeLPER.insertCriature(criature);
        Log.i("este es el id de la nueva columna",""+test);
    }
}
