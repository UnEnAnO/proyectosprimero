package com.example.fantasticanimals.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.example.fantasticanimals.Criature;
import com.example.fantasticanimals.Data.CriatureContract.CriatureEntry;

import java.util.ArrayList;

public class CriatureDBHeLPER extends SQLiteOpenHelper {
 /*
 nombre de la base de datos
 */
    private static final String DATABASE_NAME = "criature.db";

    /*
    VERSION ACTUAL DE LA BASE DE DATOS SI CAMBIAMOS EL ESQUEMA HABRA QUE ACTUALIZAR EL NUMERO
     */
    private static final int DATABASE_VERSION = 1;

    public CriatureDBHeLPER(@Nullable Context context ) { super(context, DATABASE_NAME, null, DATABASE_VERSION); }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {


       String SQL_CREATE_CRIATURE_TABLE = "CREATE TABLE " + CriatureEntry.TABLE_NAME +
               "("
               + CriatureEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
               + CriatureEntry.COLUMN_CRIATURE_NAME + " TEXT NOT NULL, "
               + CriatureEntry.COLUMN_CRIATURE_CLASIFICATIONMM + " INTEGER NOT NULL DEFAULT 1, "
               + CriatureEntry.COLUMN_CRIATURE_TIPE + " TEXT NOT NULL, "
               + CriatureEntry.COLUMN_CRIATURE_DESCRIPTION + " TEXT NOT NULL);";

       sqLiteDatabase.execSQL(SQL_CREATE_CRIATURE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void updateCriature(int id,  Criature criature){
        SQLiteDatabase db = this.getWritableDatabase();
        String UPDATE_CRIATURE = "UPDATE " + CriatureEntry.TABLE_NAME + " SET "
                + CriatureEntry.TABLE_NAME + "='" + criature.getNombre_criatura() + "', "
                + CriatureEntry.COLUMN_CRIATURE_CLASIFICATIONMM + "='" + criature.getCalificacion_criatura() + "', "
                + CriatureEntry.COLUMN_CRIATURE_TIPE + "='" + criature.getTipo_Criatura() + "', "
                + CriatureEntry.COLUMN_CRIATURE_DESCRIPTION + "='" + criature.getDescripcion_Criatura() + "'"
                + " WHERE " + CriatureEntry._ID + "=" +  id ;

        db.execSQL(UPDATE_CRIATURE);
    }


    public long insertCriature(Criature criature){

        SQLiteDatabase bd = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        // no se si hay que añadirle el id aqui tambien en principio es autoincremental
        contentValues.put(CriatureEntry.COLUMN_CRIATURE_NAME, criature.getNombre_criatura());
        contentValues.put(CriatureEntry.COLUMN_CRIATURE_CLASIFICATIONMM, criature.getCalificacion_criatura());
        contentValues.put(CriatureEntry.COLUMN_CRIATURE_TIPE, criature.getTipo_Criatura());
        contentValues.put(CriatureEntry.COLUMN_CRIATURE_DESCRIPTION, criature.getDescripcion_Criatura());

        long newRowId = bd.insert(CriatureEntry.TABLE_NAME,null,contentValues);


        return newRowId;
    }

    public ArrayList<Criature> displayDatabaseInfo(){
        ArrayList <Criature> myCriature = new ArrayList <Criature>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String [] projection = {
                CriatureEntry._ID,
                CriatureEntry.COLUMN_CRIATURE_NAME,
                CriatureEntry.COLUMN_CRIATURE_CLASIFICATIONMM,
                CriatureEntry.COLUMN_CRIATURE_TIPE,
                CriatureEntry.COLUMN_CRIATURE_DESCRIPTION
        };
        Cursor cursor = bd.query(
                CriatureEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        //obtenemos los indices de nuestras columnas
        int idCriatureColum = cursor.getColumnIndex(CriatureEntry._ID);
        int nameCriatureColum = cursor.getColumnIndex(CriatureEntry.COLUMN_CRIATURE_NAME);
        int clasificacionCriatureColum = cursor.getColumnIndex(CriatureEntry.COLUMN_CRIATURE_CLASIFICATIONMM);
        int tipoCriatureColum = cursor.getColumnIndex(CriatureEntry.COLUMN_CRIATURE_TIPE);
        int descripcionCriatureColum = cursor.getColumnIndex(CriatureEntry.COLUMN_CRIATURE_DESCRIPTION);

        //con cada uno de los indices ya podemos recorrer las filas

        while (cursor.moveToNext()){
            int currentCriatureID = cursor.getInt(idCriatureColum);
            String currentCriatureName = cursor.getString(nameCriatureColum);
            int currentCriatureClasificacion = cursor.getInt(clasificacionCriatureColum);
            String currentCriatureTipo = cursor.getString(tipoCriatureColum);
            String currentCriatureDescripcion = cursor.getString(descripcionCriatureColum);

            if (Integer.toString(currentCriatureID).isEmpty()|| currentCriatureName.isEmpty() || Integer.toString(currentCriatureClasificacion).isEmpty() || currentCriatureTipo.isEmpty() || currentCriatureDescripcion.isEmpty()){
                myCriature.add(null);
            }else{
                Criature currentCriature = new Criature(currentCriatureID,currentCriatureName,currentCriatureClasificacion,currentCriatureTipo ,currentCriatureDescripcion);
                myCriature.add(currentCriature);
            }
        }
        return myCriature;
    }
    public void deleteCriature(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String UPDATE_PETS = "DELETE FROM " + CriatureEntry.TABLE_NAME +" WHERE " + CriatureEntry._ID + "=" + id ;
        db.execSQL(UPDATE_PETS);
    }


}

