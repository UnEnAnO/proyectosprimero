package com.example.fantasticanimals.Data;

import android.provider.BaseColumns;

public class CriatureContract {

    public static final class CriatureEntry implements BaseColumns {
        public final static String TABLE_NAME = "criature";
        public final static String  _ID = "IDcriatura";
        public final static String COLUMN_CRIATURE_NAME = "nombre";
        public final static String COLUMN_CRIATURE_CLASIFICATIONMM = "clasificacionMM";
        public final static String COLUMN_CRIATURE_TIPE = "tipo";
        public final static String COLUMN_CRIATURE_DESCRIPTION = "descripcion";

    }

}
