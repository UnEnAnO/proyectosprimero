package com.example.fantasticanimals;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
// importacion de la clase criaturecontract en el main
import com.example.fantasticanimals.Data.CriatureContract.CriatureEntry;
import com.example.fantasticanimals.Data.CriatureDBHeLPER;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    ArrayList<Criature> myCriatureList = new ArrayList<Criature>();
    ImageView createCriature;
    ImageView info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CriatureDBHeLPER criatureDBHeLPER = new CriatureDBHeLPER(this);
        SQLiteDatabase db = criatureDBHeLPER.getReadableDatabase();
        createCriature = (ImageView) findViewById(R.id.createCriature);
        info = (ImageView) findViewById(R.id.info);
        ListView list = (ListView) findViewById(R.id.selectorAnimales);
        myCriatureList = criatureDBHeLPER.displayDatabaseInfo();



        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(),detallesAnimal.class);
                i.putExtra("idCRIATURE", myCriatureList.get(position).getId_criatura());
                i.putExtra("nombreCRIATURE", myCriatureList.get(position).getNombre_criatura());
                i.putExtra("clasificacionCRIATURE", Integer.toString(myCriatureList.get(position).getCalificacion_criatura()) );
                i.putExtra("tipoCRIATURE", myCriatureList.get(position).getTipo_Criatura());
                i.putExtra("descripcionCriature", myCriatureList.get(position).getDescripcion_Criatura());
                startActivity(i);
            }
        };

        createCriature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(),createAnimal.class);
                startActivity(i);
            }
        });
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(),info.class);
                startActivity(i);
            }
        });





        CriatureAdapter criatureAdapter = new CriatureAdapter(this, 0, myCriatureList);
        list.setAdapter(criatureAdapter);
        list.setOnItemClickListener(onclick);

    }
}
