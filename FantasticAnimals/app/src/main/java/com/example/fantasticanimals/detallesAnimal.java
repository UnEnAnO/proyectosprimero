package com.example.fantasticanimals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fantasticanimals.Data.CriatureDBHeLPER;

import java.util.ArrayList;

public class detallesAnimal extends AppCompatActivity {
    TextView spinnerEditCriature;
    int counter =1;
    TextView et_nameCreateCriature;
    TextView et_descriptionCreateCriature;
    TextView numeroText;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_animal);
        id = getIntent().getExtras().getInt("idCRIATURE");
        et_nameCreateCriature =findViewById(R.id.NombreFichaCriatura);
        et_descriptionCreateCriature =findViewById(R.id.descripcionEdit);

        Button btn_borrar = findViewById(R.id.btn_Borrar);
        numeroText = findViewById(R.id.nivel);
        spinnerEditCriature = findViewById(R.id.spinnerTipoCriatura);






        String hintNombreCriatura = getIntent().getStringExtra("nombreCRIATURE");
        final TextView CriatureName = (TextView) findViewById(R.id.NombreFichaCriatura);
        CriatureName.setText(hintNombreCriatura);

        String NivelCriature = getIntent().getStringExtra("clasificacionCRIATURE");
        final TextView nivelos = (TextView) findViewById(R.id.nivel);
        nivelos.setText(NivelCriature);
        counter=Integer.valueOf(NivelCriature);
        String spinnerTipoCriature = getIntent().getStringExtra("tipoCRIATURE");
        final TextView tipo = (TextView) findViewById(R.id.spinnerTipoCriatura);
        tipo.setText(spinnerTipoCriature);

        String hintdescripcion = getIntent().getStringExtra("descripcionCriature");
        final TextView descripcionCriature = (TextView) findViewById(R.id.descripcionEdit);
        descripcionCriature.setText(hintdescripcion);





        btn_borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCriature();
            }
        });

    }



    public void deleteCriature(){
        CriatureDBHeLPER criatureDBHeLPER = new CriatureDBHeLPER(this);
       criatureDBHeLPER.deleteCriature(id);
        Toast.makeText(this,"¡Criatura Eliminada Correctamente",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, MainActivity.class));
    }
}

