package com.example.fantasticanimals;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.fantasticanimals.Data.CriatureContract.CriatureEntry;
import com.example.fantasticanimals.Data.CriatureDBHeLPER;

import java.util.ArrayList;
import java.util.List;

public class CriatureAdapter extends ArrayAdapter<Criature> {

    public CriatureAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Criature> criatures) {
        super(context, resource, criatures);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View list_item = convertView;

        if (list_item == null){
            list_item = LayoutInflater.from(getContext()).inflate(R.layout.item_list, parent, false);
        }
        Criature curretCriature =getItem(position);


        TextView nombreCriatura = (TextView) list_item.findViewById(R.id.nombreCriaturaList);
        nombreCriatura.setText(curretCriature.getNombre_criatura());
        TextView calificacionCriatura = (TextView) list_item.findViewById(R.id.calificacionCriatura);
        calificacionCriatura.setText(Integer.toString(curretCriature.getCalificacion_criatura()));
        TextView tipoCriatura = (TextView) list_item.findViewById(R.id.tipoCriaturaList);
        tipoCriatura.setText(curretCriature.getTipo_Criatura());
        ImageView imagenTipo = (ImageView) list_item.findViewById(R.id.imagentipocriatura);
        if (tipoCriatura.getText().equals("Amigable")){
            imagenTipo.setImageResource(R.drawable.amigable);
        }
        if (tipoCriatura.getText().equals("Neutral")){
            imagenTipo.setImageResource(R.drawable.neutral);
        }
        if (tipoCriatura.getText().equals("Peligrosa")){
            imagenTipo.setImageResource(R.drawable.peligroso);
        }
        if (tipoCriatura.getText().equals("Potencialmente Peligrosa")){
            imagenTipo.setImageResource(R.drawable.ppeligroso);
        }


        return list_item;
    }
}
