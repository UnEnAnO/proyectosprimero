package com.example.fantasticanimals;

public class Criature {
    int id_criatura;
    String nombre_criatura;
    int calificacion_criatura;
    String tipo_Criatura;
    String descripcion_Criatura;

    public Criature(int id_criatura, String nombre_criatura, int calificacion_criatura, String tipo_Criatura, String descripcion_Criatura) {
        this.id_criatura = id_criatura;
        this.nombre_criatura = nombre_criatura;
        this.calificacion_criatura = calificacion_criatura;
        this.tipo_Criatura = tipo_Criatura;
        this.descripcion_Criatura = descripcion_Criatura;
    }

    public Criature(String nombre_criatura, int calificacion_criatura, String tipo_Criatura, String descripcion_Criatura) {
        this.nombre_criatura = nombre_criatura;
        this.calificacion_criatura = calificacion_criatura;
        this.tipo_Criatura = tipo_Criatura;
        this.descripcion_Criatura = descripcion_Criatura;
    }

    public int getId_criatura() {
        return id_criatura;
    }

    public void setId_criatura(int id_criatura) {
        this.id_criatura = id_criatura;
    }

    public String getNombre_criatura() {
        return nombre_criatura;
    }

    public void setNombre_criatura(String nombre_criatura) {
        this.nombre_criatura = nombre_criatura;
    }

    public int getCalificacion_criatura() {
        return calificacion_criatura;
    }

    public void setCalificacion_criatura(int calificacion_criatura) {
        this.calificacion_criatura = calificacion_criatura;
    }

    public String getTipo_Criatura() {
        return tipo_Criatura;
    }

    public void setTipo_Criatura(String tipo_Criatura) {
        this.tipo_Criatura = tipo_Criatura;
    }

    public String getDescripcion_Criatura() {
        return descripcion_Criatura;
    }

    public void setDescripcion_Criatura(String descripcion_Criatura) {
        this.descripcion_Criatura = descripcion_Criatura;
    }
}
