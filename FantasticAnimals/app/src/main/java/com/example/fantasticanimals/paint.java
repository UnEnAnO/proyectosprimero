package com.example.fantasticanimals;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

public class paint extends AppCompatActivity implements View.OnClickListener{
    ImageButton negro;
    ImageButton rojo;
    ImageButton azul;
    ImageButton amarillo;
    ImageButton verde;
    private static Lienzo lienzo;
    float  pincelPequenno;
    float  pincelMediano;
    float  pincelGrande;
    float pincelDefecto;

    ImageButton tPincel;
    ImageButton nuevo;
    ImageButton borrar;
    ImageButton guardar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint) ;
        // colores de los pinceles
        negro = (ImageButton)findViewById(R.id.colorNegro);
        rojo = (ImageButton)findViewById(R.id.colorRojo);
        azul = (ImageButton)findViewById(R.id.colorAzul);
        amarillo = (ImageButton)findViewById(R.id.colorAmarillo);
        verde = (ImageButton)findViewById(R.id.colorVerde);

        tPincel = (ImageButton)findViewById(R.id.tamannoPincel);
        nuevo = (ImageButton)findViewById(R.id.nuevoPaint);
        borrar = (ImageButton)findViewById(R.id.borrar);
        guardar = (ImageButton)findViewById(R.id.guardar);
        //clicado de los colores listener
        negro.setOnClickListener(this);
        rojo.setOnClickListener(this);
        azul.setOnClickListener(this);
        amarillo.setOnClickListener(this);
        verde.setOnClickListener(this);
        // clicado de las funciones listener
        tPincel.setOnClickListener(this);
        nuevo.setOnClickListener(this);
        borrar.setOnClickListener(this);
        guardar.setOnClickListener(this);
        // canvas relacionado con el xml (aqui es donde se pinta)
        lienzo = (Lienzo)findViewById(R.id.lienzo);


        // tamanno de los pinceles
        pincelPequenno = 10;
        pincelMediano = 20;
        pincelGrande = 30;
        pincelDefecto = pincelMediano;

    }

    @Override
    public void onClick(View viev) {
        String color = null;
        switch (viev.getId()){
            case R.id.colorNegro:
                color = viev.getTag().toString();
                lienzo.setColor(color);
            break;
            case R.id.colorRojo:
                color = viev.getTag().toString();
                lienzo.setColor(color);
                break;
            case R.id.colorAzul:
                color = viev.getTag().toString();
                lienzo.setColor(color);
                break;
            case R.id.colorAmarillo:
                color = viev.getTag().toString();
                lienzo.setColor(color);
                break;
            case R.id.colorVerde:
                color = viev.getTag().toString();
                lienzo.setColor(color);
                break;
            case R.id.nuevoPaint:

                AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
                newDialog.setTitle("Nuevo Dibujo");
                newDialog.setMessage("¿Comenzar nuevo dibujo (perderás el dibujo actual)?");
                newDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){

                        lienzo.NuevoDibujo();
                        dialog.dismiss();
                    }
                });
                newDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();
                    }
                });
                newDialog.show();

                break;
            case R.id.tamannoPincel:
                final Dialog tamannoPincel = new Dialog(this);
                tamannoPincel.setTitle("Tamaño del pincel:");
                tamannoPincel.setContentView(R.layout.tamannos_pincel);
                //listen for clicks on tamaños de los botones
                TextView btn_peque = (TextView)tamannoPincel.findViewById(R.id.pequenno);
                btn_peque.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {

                        Lienzo.setTamannoPincel(pincelPequenno);
                        Lienzo.setBorrado(false);
                        tamannoPincel.dismiss();
                    }
                });
                TextView btn_medi = (TextView)tamannoPincel.findViewById(R.id.mediano);
                btn_medi.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {

                        Lienzo.setTamannoPincel(pincelMediano);
                        Lienzo.setBorrado(false);
                        tamannoPincel.dismiss();
                    }
                });
                TextView btn_gran = (TextView)tamannoPincel.findViewById(R.id.grande);
                btn_gran.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //SELECCIONA EL TAMAÑO QUE LE PASAMOS EN ESTE CASO 30PX
                        Lienzo.setTamannoPincel(pincelGrande);
                        Lienzo.setBorrado(false);
                        // cierra la ventanita de seleccion al seleccionar un tamaño
                        tamannoPincel.dismiss();
                    }
                });
                // muestra la ventana y espera al que le demos un clic

                tamannoPincel.show();
                break;
            case R.id.borrar:
                final Dialog tamannoBorrador = new Dialog(this);
                tamannoBorrador.setTitle("Tamaño del borrador:");
                tamannoBorrador.setContentView(R.layout.tamannos_pincel);
                //listen for clicks on tamaños de los botones
                TextView btn_pequeBorrar = (TextView)tamannoBorrador.findViewById(R.id.pequenno);
                btn_pequeBorrar.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        Lienzo.setBorrado(true);
                        Lienzo.setTamannoPincel(pincelPequenno);
                        tamannoBorrador.dismiss();
                    }
                });
                TextView btn_mediBorrar = (TextView)tamannoBorrador.findViewById(R.id.mediano);
                btn_mediBorrar.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        Lienzo.setBorrado(true);
                        Lienzo.setTamannoPincel(pincelMediano);

                        tamannoBorrador.dismiss();
                    }
                });
                TextView btn_granBorrar = (TextView)tamannoBorrador.findViewById(R.id.grande);
                btn_granBorrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //SELECCIONA EL TAMAÑO QUE LE PASAMOS EN ESTE CASO 30PX
                        Lienzo.setBorrado(true);
                        Lienzo.setTamannoPincel(pincelGrande);
                        // cierra la ventanita de seleccion al seleccionar un tamaño
                        tamannoBorrador.dismiss();
                    }
                });
                // muestra la ventana y espera al que le demos un clic

                tamannoBorrador.show();
                break;
            case R.id.guardar:
                AlertDialog.Builder salvarDibujo = new AlertDialog.Builder(this);
                salvarDibujo.setTitle("Salvar dibujo");
                salvarDibujo.setMessage("¿Salvar Dibujo a la galeria?");
                salvarDibujo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){

                        lienzo.setDrawingCacheEnabled(true);
                        //si se salva le asignara una UUID random (que es un nombre random utilizado por todos los dispositivos) y lo guardara
                        String imgSaved = MediaStore.Images.Media.insertImage(
                                getContentResolver(), lienzo.getDrawingCache(),
                                UUID.randomUUID().toString()+".jpg", "pintura de la aplicacion de animales fantasticos");


                        if(imgSaved!=null){
                            Toast savedToast = Toast.makeText(getApplicationContext(),
                                    "¡Dibujo salvado en la galeria!", Toast.LENGTH_SHORT);
                            savedToast.show();
                        }
                        else{
                            Toast unsavedToast = Toast.makeText(getApplicationContext(),
                                    "¡Error! La imagen no ha podido ser salvada.", Toast.LENGTH_SHORT);
                            unsavedToast.show();
                        }
                        lienzo.destroyDrawingCache();
                    }
                });
                salvarDibujo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();
                    }
                });
                salvarDibujo.show();
                break;

        }
    }
}
