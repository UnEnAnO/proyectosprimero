package com.example.fantasticanimals;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.felipecsl.gifimageview.library.GifImageView;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class SplahActivity extends AppCompatActivity {
    private GifImageView gif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);
        gif = (GifImageView)findViewById(R.id.gif);

        try{
            InputStream inputStream = getAssets().open("escarbato.gif");
            byte[] bytes = IOUtils.toByteArray(inputStream);
            gif.setBytes(bytes);
            gif.startAnimation();
        }
        catch (IOException ex){

        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                SplahActivity.this.startActivity(new Intent(SplahActivity.this,MainActivity.class));
                SplahActivity.this.finish();
            }

        },3000);

    }

}
