package com.example.elimia.elvishtranslator;

import android.media.MediaPlayer;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;

public class Numeros extends AppCompatActivity {
    ArrayList<Word> myWordList = new ArrayList<Word>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numeros);
        ListView list = (ListView) findViewById(R.id.mainContainerNumeros);

        Word numeroUno = new Word("Uno", "Mîn", R.drawable.number_1, R.raw.uno);
        myWordList.add(numeroUno);
        Word numeroDos = new Word("Dos", "Tâd", R.drawable.number_2, R.raw.dos);
        myWordList.add(numeroDos);

        Word numeroTres = new Word("Tres", "Nêl", R.drawable.number_3, R.raw.tres);
        myWordList.add(numeroTres);
        Word numeroCuatro = new Word("Cuatro", "Canad", R.drawable.number_4, R.raw.cuatro);
        myWordList.add(numeroCuatro);
        Word numeroCinco = new Word("Cinco", "Leben", R.drawable.number_5, R.raw.cinco);
        myWordList.add(numeroCinco);
        Word numeroSeis = new Word("Seis", "Eneg", R.drawable.number_6, R.raw.seis);
        myWordList.add(numeroSeis);
        Word numeroSiete = new Word("Siete", "Odog", R.drawable.number_7, R.raw.siete);
        myWordList.add(numeroSiete);
        Word numeroOcho = new Word("Ocho", "Tolodh", R.drawable.number_8, R.raw.ocho);
        myWordList.add(numeroOcho);
        Word numeroNueve = new Word("Nueve", "Neder", R.drawable.number_9, R.raw.nueve);
        myWordList.add(numeroNueve);
        Word numeroDiez = new Word("Diez", "Pae", R.drawable.number_10, R.raw.diez);
        myWordList.add(numeroDiez);
        Word numeroOnce = new Word("Once", "Minig", R.drawable.number_11, R.raw.once);
        myWordList.add(numeroOnce);
        Word numeroDoce = new Word("Doce", "Uiug", R.drawable.number_12, R.raw.doce);
        myWordList.add(numeroDoce);
        Word numeroTrece = new Word("Trece", "Pae-a-nêl", R.drawable.number_13, R.raw.trece);
        myWordList.add(numeroTrece);
        Word numeroCatorce = new Word("Catorce", "Pae-a-canad", R.drawable.number_14, R.raw.catorce);
        myWordList.add(numeroCatorce);
        Word numeroQuince = new Word("Quince", "Pae-a-leben", R.drawable.number_15, R.raw.quince);
        myWordList.add(numeroQuince);
        Word numeroDieciseis = new Word("Dieciseis", "Pae-ar-eneg", R.drawable.number_16, R.raw.dieciseis);
        myWordList.add(numeroDieciseis);
        Word numeroDiecisiete = new Word("Diecisiete", "Pae-ar-odog", R.drawable.number_17, R.raw.diecisiete);
        myWordList.add(numeroDiecisiete);
        Word numeroDieciocho = new Word("Dieciocho", "Pae-a-tolodh", R.drawable.number_18, R.raw.dieciocho);
        myWordList.add(numeroDieciocho);
        Word numeroDiecinueve = new Word("Diecinueve", "Pae-a-neder", R.drawable.number_19, R.raw.diecinueve);
        myWordList.add(numeroDiecinueve);
        Word numeroVeinte = new Word("Veinte", "Taphae", R.drawable.number_20, R.raw.veinte);
        myWordList.add(numeroVeinte);

        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaPlayer pista = MediaPlayer.create(getApplicationContext(),myWordList.get(position).pista);
                pista.start();
            }
        };


        wordAdapter wordAdapter = new wordAdapter(this, 0, myWordList);
        list.setAdapter(wordAdapter);
        list.setOnItemClickListener(onclick);

/*
        ArrayList <String> numeros = new ArrayList<String>();
        numeros.add("Uno");
        numeros.add("Dos");
        numeros.add("Tres");
        numeros.add("Cuatro");
        numeros.add("Cinco");
        numeros.add("Seis");
        numeros.add("Siete");
        numeros.add("Ocho");
        numeros.add("Nueve");
        numeros.add("Diez");
        numeros.add("Once");
        numeros.add("Doce");
        numeros.add("Trece");
        numeros.add("Catorce");
        numeros.add("Quince");
        numeros.add("Dieciseis");
        numeros.add("Diecisiete");
        numeros.add("Dieciocho");
        numeros.add("Diecinueve");
        numeros.add("Veinte");

        ArrayAdapter <String> myadapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, numeros);
        ListView list = (ListView) findViewById(R.id.mainContainerNumeros);
        list.setAdapter(myadapter);
            */
    }
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.i("Entrando: ", "entre");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}