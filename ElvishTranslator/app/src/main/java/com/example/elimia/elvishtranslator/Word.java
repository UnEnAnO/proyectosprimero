package com.example.elimia.elvishtranslator;

public class Word {
    String palabraComun;
    String palabraElvish;
    int imagenId = R.drawable.number_1;
    int pista;

    public Word(String palabraComun, String palabraElvish, int imagenId, int pista) {
        this.palabraComun = palabraComun;
        this.palabraElvish = palabraElvish;
        this.imagenId = imagenId;
        this.pista = pista;
    }

    public String getPalabraComun() {
        return palabraComun;
    }

    public void setPalabraComun(String palabraComun) {
        this.palabraComun = palabraComun;
    }

    public String getPalabraElvish() {
        return palabraElvish;
    }

    public void setPalabraElvish(String palabraElvish) {
        this.palabraElvish = palabraElvish;
    }

    public int getImagenId() {
        return imagenId;
    }

    public void setImagenId(int imagenId) {
        this.imagenId = imagenId;
    }

    public int getPista() {
        return pista;
    }

    public void setPista(int pista) {
        this.pista = pista;
    }
}