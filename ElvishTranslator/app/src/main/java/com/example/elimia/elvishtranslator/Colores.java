package com.example.elimia.elvishtranslator;

import android.media.MediaPlayer;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Colores extends AppCompatActivity {
    ArrayList<Word> myWordList = new ArrayList<Word>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colores);

        ListView list = (ListView) findViewById(R.id.mainContainerColores);

        Word colorRojo = new Word("Rojo", "Caran", R.drawable.color_rojo,R.raw.rojo);
        myWordList.add(colorRojo);
        Word colorCarmin = new Word("Carmín", "Carnin", R.drawable.color_carmin, R.raw.carmin);
        myWordList.add(colorCarmin);
        Word colorAzulMarino = new Word("Azul Marino", "Luin", R.drawable.color_azulmarino, R.raw.azulmarino);
        myWordList.add(colorAzulMarino);
        Word colorAzul = new Word("Azul", "Elu", R.drawable.color_azul, R.raw.azul);
        myWordList.add(colorAzul);
        Word colorAmarillo = new Word("Amarillo", "Malen", R.drawable.color_amarillo, R.raw.amarillo);
        myWordList.add(colorAmarillo);
        Word colorVerde = new Word("Verde", "Calen", R.drawable.color_verde, R.raw.verde);
        myWordList.add(colorVerde);
        Word colorNegro = new Word("Negro", "Morn", R.drawable.color_negro, R.raw.negro);
        myWordList.add(colorNegro);
        Word colorMarronOscuro = new Word("Marron Oscuro", "Baran", R.drawable.color_marronoscuro, R.raw.marronoscuro);
        myWordList.add(colorMarronOscuro);
        Word colorMarron = new Word("Marron", "Rhosg", R.drawable.color_marron, R.raw.marron);
        myWordList.add(colorMarron);
        Word colorBlanco = new Word("Blanco", "Faen", R.drawable.color_blanco, R.raw.blanco);
        myWordList.add(colorBlanco);
        Word colorPalido = new Word("Palido", "Nimp", R.drawable.color_palido, R.raw.palido);
        myWordList.add(colorPalido);
        Word colorGris = new Word("Gris", "Mith", R.drawable.color_gris, R.raw.gris);
        myWordList.add(colorGris);
        Word colorNaranja = new Word("Naranja", "Cull", R.drawable.color_naranja, R.raw.naranja);
        myWordList.add(colorNaranja);
        Word colorRosa = new Word("Rosa", "Crinth", R.drawable.color_rosa, R.raw.rosa);
        myWordList.add(colorRosa);
        Word colorVioleta = new Word("Violeta", "Lirg", R.drawable.color_violeta, R.raw.violeta);
        myWordList.add(colorVioleta);

        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaPlayer pista = MediaPlayer.create(getApplicationContext(),myWordList.get(position).pista);
                pista.start();
            }
        };

        wordAdapter wordAdapter = new wordAdapter(this, 0, myWordList);
        list.setAdapter(wordAdapter);
        list.setOnItemClickListener(onclick);
    }
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.i("Entrando: ", "entre");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
