package com.example.elimia.elvishtranslator;

import android.media.MediaPlayer;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class Frases extends AppCompatActivity {
    ArrayList<Word> myWordList = new ArrayList<Word>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frases);

        ListView list = (ListView) findViewById(R.id.mainContainerFrases);

        Word Agua = new Word("Agua", "Nen", R.drawable.agua, R.raw.agua);
        myWordList.add(Agua);
        Word Arco = new Word("Arco", "Lúva", R.drawable.arco, R.raw.arco);
        myWordList.add(Arco);
        Word Caballo = new Word("Caballo", "Roch", R.drawable.caballo, R.raw.caballo);
        myWordList.add(Caballo);
        Word Capa = new Word("Capa", "Collo", R.drawable.capa, R.raw.capa);
        myWordList.add(Capa);
        Word Descanso = new Word("Descanso", "Estë", R.drawable.descanso, R.raw.descanso);
        myWordList.add(Descanso);
        Word hermana = new Word("Dragón", "Urulóki", R.drawable.dragon, R.raw.dragon);
        myWordList.add(hermana);
        Word Fiesta = new Word("Fiesta", "Mereth", R.drawable.fiesta, R.raw.fiesta);
        myWordList.add(Fiesta);
        Word magia = new Word("Magia negra", "Morgul", R.drawable.magia, R.raw.magia);
        myWordList.add(magia);



        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaPlayer pista = MediaPlayer.create(getApplicationContext(),myWordList.get(position).pista);
                pista.start();
            }
        };

        wordAdapter wordAdapter = new wordAdapter(this, 0, myWordList);
        list.setAdapter(wordAdapter);
        list.setOnItemClickListener(onclick);

    }




    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.i("Entrando: ", "entre");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
