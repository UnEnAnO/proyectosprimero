package com.example.elimia.elvishtranslator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class wordAdapter extends ArrayAdapter<Word> {


    public wordAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Word> numeros) {
        super(context, resource, numeros);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View list_item = convertView;
        //verificamos si la vista esta vacia para en caso afirmativo inflarla
        if(list_item == null){
            list_item = LayoutInflater.from(getContext()).inflate(R.layout.item_list, parent, false);
        }

        //nos traemos cada una de los numeros de nuestro Array
        Word currentWord = getItem(position);
        TextView wordEs = (TextView) list_item.findViewById(R.id.wordEs);
        wordEs.setText(currentWord.getPalabraComun());
        TextView wordElf = (TextView) list_item.findViewById(R.id.wordElf);
        wordElf.setText(currentWord.getPalabraElvish());
        ImageView imageView = (ImageView) list_item.findViewById(R.id.imagenWord);
        imageView.setImageResource(currentWord.getImagenId());



        return list_item;
    }
}
