package com.example.elimia.elvishtranslator;

import android.media.MediaPlayer;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Familia extends AppCompatActivity {
    ArrayList<Word> myWordList = new ArrayList<Word>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_familia);

        ListView list = (ListView) findViewById(R.id.mainContainerFamilia);

        Word madre = new Word("Madre", "Naneth", R.drawable.family_madre, R.raw.madreaudio);
        myWordList.add(madre);

        Word padre = new Word("Padre", "Adanadar", R.drawable.family_padre, R.raw.padreaudio);
        myWordList.add(padre);
        Word hijo = new Word("Hijo", "Ionn", R.drawable.family_hijo, R.raw.hijoaudio);
        myWordList.add(hijo);
        Word hija = new Word("Hija", "Sell", R.drawable.family_hija, R.raw.hijaaudio);
        myWordList.add(hija);
        Word hermano = new Word("Hermano", "Muindor", R.drawable.family_hermano, R.raw.hermanoaudio);
        myWordList.add(hermano);
        Word hermana = new Word("Hermana", "Muinthel", R.drawable.family_hermana, R.raw.hermanaaudio);
        myWordList.add(hermana);
        Word amigo = new Word("Amigo", "Mellon", R.drawable.family_amigo, R.raw.amigoaudio);
        myWordList.add(amigo);
        Word Enano = new Word("Enano", "Nogoth", R.drawable.enano, R.raw.enano);
        myWordList.add(Enano);
        Word Elfo = new Word("Elfo", "Edhel", R.drawable.elfo, R.raw.elfo);
        myWordList.add(Elfo);


        AdapterView.OnItemClickListener onclick = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaPlayer pista = MediaPlayer.create(getApplicationContext(),myWordList.get(position).pista);
                pista.start();
            }
        };

        wordAdapter wordAdapter = new wordAdapter(this, 0, myWordList);
        list.setAdapter(wordAdapter);
        list.setOnItemClickListener(onclick);

    }
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.i("Entrando: ", "entre");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}