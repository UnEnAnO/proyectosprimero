package com.example.elimia.elvishtranslator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView numeros = (TextView) findViewById(R.id.numeros);
        TextView familia = (TextView) findViewById(R.id.familia);
        TextView  colores = (TextView) findViewById(R.id.colores);
        TextView frases = (TextView) findViewById(R.id.frases);
        final ImageView imagenInicio = (ImageView) findViewById(R.id.inicio);

        View.OnClickListener OnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View viev) {
                Intent i = new Intent();
                if (viev.getId()== R.id.numeros){
                    i = new Intent(getApplicationContext(), Numeros.class);

                }
                if (viev.getId()== R.id.familia){
                    i = new Intent(getApplicationContext(), Familia.class);

                }
                if (viev.getId()== R.id.colores){
                    i = new Intent(getApplicationContext(), Colores.class);

                }
                if (viev.getId()== R.id.frases){
                    i = new Intent(getApplicationContext(), Frases.class);

                }
                startActivity(i);
            }
        };
        numeros.setOnClickListener(OnClickListener);
        familia.setOnClickListener(OnClickListener);
        colores.setOnClickListener(OnClickListener);
        frases.setOnClickListener(OnClickListener);
        imagenInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            imagenInicio.setVisibility(View.GONE);
            }
        });

    }

}
